import { Module } from '@nestjs/common';
import { AlamatService } from './alamat.service';
import { AlamatController } from './alamat.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/users.entity';
import { Alamat } from './entities/alamat.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Alamat])],
  providers: [AlamatService],
  controllers: [AlamatController]
})
export class AlamatModule {}
