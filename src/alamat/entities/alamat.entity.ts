import { Order } from "src/order/entities/order.entity";
import { User } from "src/users/entities/users.entity";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Alamat {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    kota: string

    @Column()
    provinsi: string

    @Column()
    detail: string

    @ManyToOne(() => User, user => user.alamat)
    user: User

    @OneToMany(() => Order, order => order.alamat)
    order: Alamat[]
}