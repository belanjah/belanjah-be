import { Module } from '@nestjs/common';
import { KategoriService } from './kategori.service';
import { KategoriController } from './kategori.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Produk } from 'src/produk/entities/produk.entity';
import { Kategori } from './entities/kategori.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Produk, Kategori])],
  providers: [KategoriService],
  controllers: [KategoriController]
})
export class KategoriModule {}
