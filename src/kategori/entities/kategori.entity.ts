import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Kategori {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    kategori: string
}