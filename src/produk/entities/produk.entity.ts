import { Keranjang } from "src/keranjang/entities/keranjang.entity";
import { Order } from "src/order/entities/order.entity";
import { User } from "src/users/entities/users.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Produk {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    productName: string

    @Column()
    gambar: string

    @Column()
    harga: number

    @Column()
    stok: number

    @ManyToOne(() => User, user => user.produk)
    user: User

    @ManyToOne(() => Keranjang, keranjang => keranjang.produk)
    keranjang: Keranjang[]

    @ManyToOne(() => Order, order => order.produk)
    order: Order[]
}