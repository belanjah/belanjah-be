import { Module } from '@nestjs/common';
import { ProdukService } from './produk.service';
import { ProdukController } from './produk.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/users.entity';
import { Produk } from './entities/produk.entity';
import { Kategori } from 'src/kategori/entities/kategori.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Produk, Kategori])
  ],
  providers: [ProdukService],
  controllers: [ProdukController]
})
export class ProdukModule {}
