import { Alamat } from "src/alamat/entities/alamat.entity";
import { Keranjang } from "src/keranjang/entities/keranjang.entity";
import { Order } from "src/order/entities/order.entity";
import { Produk } from "src/produk/entities/produk.entity";
import { Role } from "src/role/entities/role.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column({unique: true})
    username: string

    @Column({unique: true})
    email: string

    @Column()
    password: string

    @Column()
    noHp: number

    @Column()
    photoProfile: string

    @CreateDateColumn({
        type: 'timestamp',
        nullable: false
    })
    CreatedAt: Date

    @ManyToOne(() => Role, role => role.users)
    roleId: Role

    @OneToMany(() => Alamat, alamat => alamat.user)
    alamat: Alamat[]

    @OneToMany(() => Produk, produk => produk.user)
    produk: Produk[]

    @OneToMany(() => Keranjang, keranjang => keranjang.user)
    keranjang: Keranjang[]

    @ManyToOne(() => Order, order => order.user)
    order: Order[]
}