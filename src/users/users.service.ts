import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/users.entity';
import { Repository } from 'typeorm';
import { Role } from 'src/role/entities/role.entity';
import { UserDto } from './dto/user.dto';
import * as bcrypt from 'bcrypt';
import { UpdateRoleDto } from './dto/updateRole.dto';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,

        @InjectRepository(Role)
        private roleRepository: Repository<Role>,
    ) {}

    async updateRole(id: string, updateRoleDto : UpdateRoleDto) {
        const roleData = await this.roleRepository.findOne({
            where: {
                id: updateRoleDto.roleId
            }
        })

        const data = new User()
        data.roleId = roleData

        return await this.userRepository.update(id, data)
    }

    async create(userDto : UserDto) {
        try {
            const roleData = await this.roleRepository.findOne({
                where: {
                    id: userDto.role
                }
            })

            const hash = await bcrypt.hash(userDto.password, 12)

            const data = new User()
            data.username = userDto.username
            data.email = userDto.email
            data.password = hash
            data.roleId = roleData

            const result = await this.userRepository.insert(data)

            return {
                data : await this.userRepository.findOneOrFail({
                    where: {
                        id: result.identifiers[0].id
                    },
                    relations: {
                        roleId: true
                    }
                }),
                statusCode: HttpStatus.CREATED,
                message: 'User Created'
            }

        } catch (error) {
            const usernameFound = await this.userRepository.findOne({
                where: {
                    username: userDto.username
                }
            })

            const emailFound = this.userRepository.findOne({
                where: {
                    email: userDto.email
                },
                relations: {
                    roleId: true
                }
            })

            if (usernameFound !== null) {
                return {
                    username: userDto.username,
                    message: 'User Found',
                    statusCode: HttpStatus.FOUND
                }
            } 

            if (emailFound !== null) {
                return {
                    email: userDto.email,
                    message: 'Email Found',
                    statusCode: HttpStatus.FOUND
                }
            }
        }
    }

    async getAll() {
        try {
            return await this.userRepository.findAndCount({
                relations: {
                    roleId: true
                }
            })
        } catch (error) {
            
        }
    }

    async getById(id: string) {
        try {
            return await this.userRepository.findOneOrFail({
                where: {
                    id
                }, 
                relations: {
                    roleId: true
                }
            })
        } catch (error) {
            return new HttpException(
                {
                    statusCode: HttpStatus.NOT_FOUND,
                    error: 'Data Not Found'
                },
                HttpStatus.NOT_FOUND
            ) 
        }
    }

    async delete(id: string) {
        try {
            await this.userRepository.delete(id)
            return {
                message: "Success Deleted",
                statusCode: HttpStatus.OK
            }
        } catch (error) {
            return new HttpException(
                {
                    statusCode: HttpStatus.NOT_FOUND,
                    error: 'Data Not Found'
                },
                HttpStatus.NOT_FOUND
            ) 
        }
    }

}
