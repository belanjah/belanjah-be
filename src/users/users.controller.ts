import { Controller, Post, Get, Delete, Put, Body, HttpStatus, Param, ParseUUIDPipe } from '@nestjs/common';
import { UsersService } from './users.service';
import { UserDto } from './dto/user.dto';
import { UpdateRoleDto } from './dto/updateRole.dto';

@Controller('users')
export class UsersController {

    constructor(
        private userService: UsersService
    ) {}

    @Post()
    async create(@Body() req: UserDto) {
        return await this.userService.create(req)
    }

    @Get()
    async getAll() {
        const [data, count] = await this.userService.getAll()

        return {
            data,
            count,
        }
    }

    @Get('/:id')
    async getById(@Param('id', ParseUUIDPipe) id: string) {
        return this.userService.getById(id)
    }

    @Delete('/:id')
    async delete(@Param('id', ParseUUIDPipe) id: string) {
        return this.userService.delete(id)
    }

    @Put('/:id')
    async updateRole(
        @Param('id', ParseUUIDPipe) id: string,
        @Body() req: UpdateRoleDto) {
        return this.userService.updateRole(id, req)
    }

}
