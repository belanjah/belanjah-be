import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from 'src/role/entities/role.entity';
import { User } from './entities/users.entity';
import { Alamat } from 'src/alamat/entities/alamat.entity';
import { Produk } from 'src/produk/entities/produk.entity';
import { Keranjang } from 'src/keranjang/entities/keranjang.entity';
import { Order } from 'src/order/entities/order.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Role, User, Alamat, Produk, Keranjang, Order])
  ],
  providers: [UsersService],
  controllers: [UsersController]
})
export class UsersModule {}
