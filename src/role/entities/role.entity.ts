import { User } from "src/users/entities/users.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Role {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column({unique: true})
    role: string

    @OneToMany(() => User, user => user.roleId)
    users: User[]
}