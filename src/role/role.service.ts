import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from './entities/role.entity';
import { Repository } from 'typeorm';
import { RoleDto } from './dto/role.dto';
@Injectable()
export class RoleService {

    constructor(
        @InjectRepository(Role)
        private roleRepository: Repository<Role>
    ) {}

    async create(roleDto: RoleDto) {
        try {
            const data = new Role()
            data.role = roleDto.role

            const result = await this.roleRepository.insert(data)

            return await this.roleRepository.findOne({
                where: {
                    id: result.identifiers[0].id
                }
            })
        } catch (error) {
            console.log(error, "ini adalah errornya")
        }
    }

    async getAll() {
        try {
            return await this.roleRepository.findAndCount()
        } catch (error) {
            console.log(error, "ini adalah errornya")
        }
    }

}
