import { Controller, Post, Body, Get, HttpStatus } from '@nestjs/common';
import { RoleService } from './role.service';
import { RoleDto } from './dto/role.dto';

@Controller('role')
export class RoleController {

    constructor(
        private roleService: RoleService
    ) {}

    @Post()
    async create(@Body() req: RoleDto) {
        return await this.roleService.create(req)
    }

    @Get()
    async getAll() {
        const [data, count] = await this.roleService.getAll()

        return {
            data,
            count,
            statusCode: HttpStatus.OK,
            message: 'Success'
        }
    }
        
}
