import { Module } from '@nestjs/common';
import { KeranjangService } from './keranjang.service';
import { KeranjangController } from './keranjang.controller';

@Module({
  providers: [KeranjangService],
  controllers: [KeranjangController]
})
export class KeranjangModule {}
