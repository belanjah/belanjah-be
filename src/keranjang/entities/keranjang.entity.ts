import { Produk } from "src/produk/entities/produk.entity";
import { User } from "src/users/entities/users.entity";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Keranjang {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    amount: number

    @Column()
    totalPrice: number

    @OneToMany(() => Produk, produk => produk.keranjang)
    produk: Produk

    @ManyToOne(() => User, user => user.keranjang)
    user: User  
}