import { Alamat } from "src/alamat/entities/alamat.entity";
import { Produk } from "src/produk/entities/produk.entity";
import { User } from "src/users/entities/users.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Order {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    amount: number

    @Column()
    totalPrice: number

    @Column()
    terbayar: boolean

    @CreateDateColumn({
        type: 'timestamp',
        nullable: false
    })
    OrderAt: Date

    @OneToMany(() => Produk, produk => produk.order)
    produk: Produk

    @ManyToOne(() => User, user => user.order)
    user: User

    @ManyToOne(() => Alamat, alamat => alamat.order)
    alamat: Alamat
}